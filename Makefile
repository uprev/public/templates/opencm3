PROJECT = blinky
BUILD_DIR = bin


CFILES = src/main.c

# TODO - you will need to edit these two lines!
DEVICE=stm32f401ccu6
OOCD_FILE = board/stm32f4discovery.cfg




# Include the libOpenCM3 makefiles
-include $(OPENCM3_DIR)/include.mk

#This section adds in docker targets 
DEVCONTAINER_IMAGE ?= uprev/opencm3

.PHONY: docker-build docker-mount 

#Mount Open a shell in the devcontainer and mount project 
docker-mount:
	docker run -it --mount src=$(PWD),target=/workspace,type=bind -w /workspace $(DEVCONTAINER_IMAGE) /bin/bash

#Run build in the devcontainer
docker-build:
	docker run -it --mount src=$(PWD),target=/workspace,type=bind -w /workspace $(DEVCONTAINER_IMAGE) make


